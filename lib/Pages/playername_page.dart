import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mafia_bazi/BloC/player_name_cubit.dart';
import 'package:mafia_bazi/Models/PlayersName.dart';
import 'package:mafia_bazi/Repositories/player_name_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'character_page.dart';

class PlayersNames extends StatelessWidget {
  const PlayersNames({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BlocProvider(
          create: (context) => PlayerCubits()..getPlayer(),
          child: PlayerNameBody()),
    );
  }
}

class PlayerNameBody extends StatefulWidget {
  const PlayerNameBody({Key? key}) : super(key: key);

  @override
  State<PlayerNameBody> createState() => _PlayerNameBodyState();
}

class _PlayerNameBodyState extends State<PlayerNameBody> {
  final _repository = PlayerRepository();
  int CurrentPlayers = 0;

  // final Future<List<PlayersName>> names = _repository.getPlayers();
  final _nameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            'PLAYERS (${CurrentPlayers})',
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
          elevation: 0,
          actions: [
            Padding(
                padding: const EdgeInsets.all(8.0),
                child: IconButton(
                    onPressed: () {
                      // if (CurrentPlayers > 6) {
                      //   Navigator.push(
                      //     context,
                      //     MaterialPageRoute(
                      //         builder: (context) => CharactersPage()),
                      //   );
                      // } else {
                      //   ShowToast(CurrentPlayers);
                      // }
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CharactersPage()),
                      );
                    },
                    icon: const Icon(
                      Icons.check,
                      color: Colors.black,
                    )))
          ],
        ),
        body: BlocBuilder<PlayerCubits, PlayerState>(
          builder: (context, state) {
            if (state is ListPlayers) {
              return isNotEmpty(state.players);
            } else if (state is PlayersFailure) {
              return Center(
                child: Text('something went wrong'),
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ));
  }

  Widget isNotEmpty(List<PlayersName> playerss) {
    List<PlayersName> players = playerss.reversed.toList();
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        players.isEmpty
            ? isEmpty()
            : Expanded(
                child: ListView.builder(
                    physics: ScrollPhysics(),
                    itemCount: players.length,
                    itemBuilder: (context, index) {
                      return Card(
                        child: ListTile(
                          title: Text(players[index].Players.toString()),
                          leading: Icon(Icons.person),
                          trailing: InkWell(
                              onTap: () {
                                setState(() {
                                  //_repository.removePlayer(players[index]);
                                  BlocProvider.of<PlayerCubits>(context)
                                      .removePlayers(players[index]);
                                  CurrentPlayers = playerss.length;
                                });
                              },
                              child: Icon(Icons.cancel)),
                        ),
                      );
                    })),
        Positioned(
            right: 0,
            left: 0,
            bottom: 100,
            child: Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: TextField(
                        textAlign: TextAlign.center,
                        expands: false,
                        controller: _nameController,
                        decoration: InputDecoration(
                          labelText: 'player name',
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                    icon: const Icon(Icons.person_add_rounded),
                    onPressed: () {
                      setState(() {
                        if (_nameController.text != '') {
                          setState(() {
                            BlocProvider.of<PlayerCubits>(context)
                                .addPlayer(_nameController.text);
                          });
                          CurrentPlayers = ++CurrentPlayers;
                        }
                        _nameController.text = '';
                      });
                    },
                  ),
                )
              ],
            ))
      ],
    );
  }

  void ShowToast(int lengh) => Fluttertoast.showToast(
      msg: 'add ${7 - lengh} more player!', fontSize: 18);

  Widget isEmpty() {
    return Expanded(
      child: Center(
        child: Text('there is no name!'),
      ),
    );
  }

  Widget ListItems(List<PlayersName> player, int index) {
    return Card(
      child: ListTile(
        title: Text(player[index].Players.toString()),
        leading: Icon(Icons.person),
        trailing: InkWell(
            onTap: () {
              setState(() {
                setState(() {
                  _repository.removePlayer(player[index]);
                });
              });
            },
            child: Icon(Icons.cancel)),
      ),
    );
  }
}
