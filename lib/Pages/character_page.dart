import 'package:flutter/material.dart';
import 'package:mafia_bazi/Models/characters_model.dart';
import 'package:mafia_bazi/Pages/show_chars.dart';

class CharactersPage extends StatefulWidget {
  @override
  State<CharactersPage> createState() => _CharactersPageState();
}

class _CharactersPageState extends State<CharactersPage> {
  int playersLength=0;

  //_CharactersPageState(this.playersLength);

  List<CharModel> characters = [
    CharModel('شهر ساده', 'شهروند', false),
    CharModel('تک تیرانداز', 'شهروند', false),
    CharModel('نگهبان', 'شهروند', false),
    CharModel('کشیش', 'شهروند', false),
    CharModel('رویین تن', 'شهروند', false),
    CharModel('ساقی', 'شهروند', false),
    CharModel('گان اسمیت', 'شهروند', false),
    CharModel('تکاور', 'شهروند', false),
    CharModel('کابوی', 'شهروند', false),
    CharModel('هکر', 'شهروند', false),
    CharModel('دکتر', 'شهروند', false),
    CharModel('ماسون', 'شهروند', false),
    CharModel('تایلر', 'شهروند', false),
    CharModel('کارگاه', 'شهروند', false),
    CharModel('انتی لیدی', 'شهروند', false),
    CharModel('جان ساخت', 'شهروند', false),
    ////////////////////////////////////////////////
    CharModel('گادفادر', 'مافیا', false),
    CharModel('سائوچر', 'مافیا', false),
    CharModel('ناتاشا', 'مافیا', false),
    CharModel('تروریست', 'مافیا', false),
    CharModel('لیدی وودو', 'مافیا', false),
    CharModel('مذاکره', 'مافیا', false),
    CharModel('ناتو', 'مافیا', false),
    CharModel('کتر', 'مافیا', false),
    CharModel('مافیا ساده', 'مافیا', false),
    CharModel('جاسوس', 'مافیا', false),
    ////////////////////////////////////////////////
    CharModel('کیلر', 'مستقل', false),
    CharModel('فن کیلر', 'مستقل', false),
    CharModel('بمبر', 'مستقل', false),
    CharModel('مرد روسی', 'مستقل', false),
    CharModel('گمبلر', 'مستقل', false),
  ];

  List<CharModel> selectedChars = [];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          leading: InkWell(
            child: Icon(Icons.arrow_back_ios),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          title: Text(
              'select character(${playersLength}/${selectedChars.length})'),
        ),
        body: SafeArea(
          child: Column(
            children: [
              Expanded(
                child: ListView.builder(
                    itemCount: characters.length,
                    itemBuilder: (context, index) {
                      return CharItem(
                          characters[index].name,
                          characters[index].side,
                          characters[index].isSelected,
                          index);
                    }),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                child: SizedBox(
                  width: double.infinity,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ShowChars(
                                 // PlayersNames: widget.names,
                                  SelectedChars: selectedChars,
                                )),
                      );
                    },
                    child: Center(
                      child: Text('add ${selectedChars.length} character'),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget CharItem(String name, String side, bool isSelected, int index) {
    return ListTile(
      leading: const CircleAvatar(
        child: Icon(
          Icons.person,
          color: Colors.white,
        ),
        backgroundColor: Colors.lightBlueAccent,
      ),
      title: Text(name),
      subtitle: Text(side, style: const TextStyle(color: Colors.red)),
      trailing: isSelected
          ? const Icon(
              Icons.check_circle_rounded,
              color: Colors.lightBlueAccent,
            )
          : const Icon(
              Icons.check_circle_outline,
              color: Colors.lightBlueAccent,
            ),
      onTap: () {
        // if (playersLength - 1 >= selectedChars.length) {
        //   setState(() {
        //     characters[index].isSelected = !characters[index].isSelected;
        //     if (characters[index].isSelected == true) {
        //       selectedChars.add(CharModel(name, side, true));
        //     } else if (characters[index].isSelected == false) {
        //       selectedChars.removeWhere(
        //           (element) => element.name == characters[index].name);
        //     }
        //   });
        // }
        setState(() {
          characters[index].isSelected = !characters[index].isSelected;
          if (characters[index].isSelected == true) {
            selectedChars.add(CharModel(name, side, true));
          } else if (characters[index].isSelected == false) {
            selectedChars.removeWhere(
                    (element) => element.name == characters[index].name);
          }
        });
      },
    );
  }
}
