import 'package:flutter/material.dart';

class CurrentCharacters extends StatefulWidget {
  const CurrentCharacters({Key? key}) : super(key: key);

  @override
  State<CurrentCharacters> createState() => _CurrentCharactersState();
}

class _CurrentCharactersState extends State<CurrentCharacters> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: 12,
        itemBuilder: (context,index){
      return listItems();
    });
  }
  Widget listItems(){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 5),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Colors.black12),
            color: Colors.white10
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 20,horizontal: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              Text('char (name)'),
              Text('side'),
              Icon(Icons.cancel)
            ],
          ),
        ),
      ),
    );
  }
}
