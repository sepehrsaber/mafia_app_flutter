import 'package:flutter/material.dart';

class StatusQuery extends StatelessWidget {
  const StatusQuery({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: 12,
        itemBuilder: (context,index){
          return listItems();
        });
  }
  Widget listItems(){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 5),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Colors.black12),
            color: Colors.white10
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 20,horizontal: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('name'),
              Text('char'),
              Text('side'),
            ],
          ),
        ),
      ),
    );
  }
}