import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mafia_bazi/BloC/show_chars_cubit.dart';

class ShowAllChars extends StatelessWidget {
  const ShowAllChars({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: BlocProvider(
          create: (context) => ShowAttributesCubit()..getAttribute(),
          child: Scaffold(
            body: BlocBuilder<ShowAttributesCubit, ShowCharsState>(
              builder: (context, state) {
                if (state is ListAttributes) {
                  if (state.chars.isEmpty) {
                    return Center(
                      child: Text('empty'),
                    );
                  } else {
                    return ListView.builder(
                        itemCount: state.chars.length,
                        itemBuilder: (context, index) {
                          return listItems(
                              state.chars[index].PlayerName,
                              state.chars[index].Character,
                              state.chars[index].Side);
                        });
                  }
                } else if (state is LoadFailure) {
                  print(state.error);
                  return Center(
                    child: Text(state.error),
                  );
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            ),
          ),
        ));
  }

  Widget listItems(String? name, String? char, String? side) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Colors.black12),
            color: Colors.white10),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(name!),
              Text(char!),
              Text(side!),
            ],
          ),
        ),
      ),
    );
  }
}
