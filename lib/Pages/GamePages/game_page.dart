import 'package:flutter/material.dart';
import 'package:mafia_bazi/Pages/GamePages/current_characters.dart';
import 'package:mafia_bazi/Pages/GamePages/end_page.dart';
import 'package:mafia_bazi/Pages/GamePages/night_events.dart';
import 'package:mafia_bazi/Pages/GamePages/show_all_chars.dart';
import 'package:mafia_bazi/Pages/GamePages/status_query.dart';

class GamePageMain extends StatefulWidget {
  GamePageMain({Key? key}) : super(key: key);

  @override
  State<GamePageMain> createState() => _GamePageMainState();
}

class _GamePageMainState extends State<GamePageMain> {
  int indexs = 0;
  final screens = [
    const CurrentCharacters(),
    const NightEvents(),
    const StatusQuery(),
    const ShowAllChars(),
    const EndGamePage(),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: screens[indexs],
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: indexs,
          type: BottomNavigationBarType.fixed,
          onTap: (index) {
            setState(() {
              indexs = index;
            });
          },
          items: const [
            BottomNavigationBarItem(
                icon: Icon(Icons.shuffle),
                label: 'current',
                backgroundColor: Colors.blueAccent),
            BottomNavigationBarItem(
                icon: Icon(Icons.shuffle),
                label: 'night events',
                backgroundColor: Colors.blueAccent),
            BottomNavigationBarItem(
                icon: Icon(Icons.shuffle),
                label: 'status',
                backgroundColor: Colors.blueAccent),
            BottomNavigationBarItem(
                icon: Icon(Icons.shuffle),
                label: 'main dek',
                backgroundColor: Colors.blueAccent),
            BottomNavigationBarItem(
                icon: Icon(Icons.shuffle),
                label: 'end',
                backgroundColor: Colors.blueAccent),
          ],
        ),
      ),
    );
  }
}
