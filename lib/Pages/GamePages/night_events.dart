import 'package:flutter/material.dart';

class NightEvents extends StatefulWidget {
  const NightEvents({Key? key}) : super(key: key);

  @override
  State<NightEvents> createState() => _NightEventsState();
}

class _NightEventsState extends State<NightEvents> {
  int nights = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
          itemCount: nights,
          itemBuilder: (context,index){
        return nightItem();
      }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            nights = ++nights;
          });
        },
        child: Icon(Icons.add),
      ),
    );
  }
  Widget nightItem(){
    return Padding(padding: EdgeInsets.symmetric(vertical:2,horizontal: 5),
    child: Card(
      child: SizedBox(
        width: double.infinity,
        height: 100,
      ),
    ),
    );
  }
}
