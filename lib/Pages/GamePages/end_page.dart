import 'package:flutter/material.dart';

class EndGamePage extends StatefulWidget {
  const EndGamePage({Key? key}) : super(key: key);

  @override
  State<EndGamePage> createState() => _EndGamePageState();
}

class _EndGamePageState extends State<EndGamePage> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: RaisedButton(
        onPressed: (){},
        child: Text('end game'),
      ),
    );
  }
}
