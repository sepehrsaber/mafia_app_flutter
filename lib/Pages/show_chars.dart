import 'dart:math';

import 'package:flutter/material.dart';
import 'package:mafia_bazi/BloC/show_chars_cubit.dart';
import 'package:mafia_bazi/Models/AttributedChars.dart';
import 'package:mafia_bazi/Models/PlayersName.dart';
import 'package:mafia_bazi/Models/characters_model.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'GamePages/game_page.dart';

class ShowChars extends StatefulWidget {
  //List<String> PlayersNames;
  List<CharModel> SelectedChars;

  ShowChars({required this.SelectedChars, Key? key}) : super(key: key);

  @override
  State<ShowChars> createState() => _ShowCharsState();
}

class _ShowCharsState extends State<ShowChars> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BlocProvider(
        create: (context) => ShowAttributesCubit()..getAttribute(),
        child: Scaffold(
          body: BlocBuilder<ShowAttributesCubit, ShowCharsState>(
            builder: (context, state) {
              if (state is ListAttributes) {
                return ShowBody(
                  SelectedChars: widget.SelectedChars,
                  PlayersNames: state.names,
                  attr: state.chars,
                );
              } else if (state is LoadFailure) {
                print(state.error);
                return Center(
                  child: Text(state.error),
                );
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          ),
        ),
      ),
    );
  }
}

class ShowBody extends StatefulWidget {
  final List<CharModel> SelectedChars;

  final List<AttributedChars> attr;

  final List<PlayersName> PlayersNames;

  ShowBody(
      {required this.SelectedChars,
      required this.attr,
      required this.PlayersNames});

  @override
  State<ShowBody> createState() => _ShowBodyState();
}

class _ShowBodyState extends State<ShowBody> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        widget.SelectedChars.isNotEmpty
            // ? notEmpty(widget.PlayersNames, widget.SelectedChars)
            ? Expanded(
                child: GridView.count(
                  crossAxisCount: 3,
                  children: List.generate(widget.PlayersNames.length, (index) {
                    return InkWell(
                      onTap: () {
                        _showMyDialog(
                            context, widget.SelectedChars[index].name);
                        setState(() {
                          widget.PlayersNames.remove(
                              widget.PlayersNames[index]);
                          widget.SelectedChars.remove(
                              widget.SelectedChars[index]);
                        });
                      },
                      child: Card(
                        child: Center(
                          child: Text(
                              widget.PlayersNames[index].Players.toString()),
                        ),
                      ),
                    );
                  }),
                ),
              )
            : Expanded(child: Center(child: EmptyList()))
      ],
    );
  }

  Widget EmptyList() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text('All playes have a character'),
          ElevatedButton(
            onPressed: () {
              print('this code is compiling');
              //   BlocProvider.of<ShowAttributesCubit>(context).addAttribute(
              //       PlayersNames[index],
              //       SelectedChars[index].name,
              //       SelectedChars[index].side);
              // });
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => GamePageMain()),
              );
            },
            child: Text('Let\'s Go!'),
          )
        ],
      ),
    );
  }

  Widget notEmpty(List<String> PlayersNames, List<CharModel> SelectedChars) {
    setState(() {
      // List.generate(PlayersNames.length, (index){
      //   BlocProvider.of<ShowAttributesCubit>(context).addAttribute(
      //       PlayersNames[index],
      //       SelectedChars[index].name,
      //       SelectedChars[index].side);
      // });
      // print('this code is compiling');
    });
    return GridView.count(
      crossAxisCount: 3,
      children: List.generate(widget.PlayersNames.length, (index) {
        return InkWell(
          onTap: () {
            _showMyDialog(context, widget.SelectedChars[index].name);
            print('this code is compiling');
          },
          child: Card(
            child: Center(
              child: Text(widget.PlayersNames[index].Players.toString()),
            ),
          ),
        );
      }),
    );
  }

  Future<void> _showMyDialog(BuildContext context, String char) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('player character:'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(char),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
