import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:mafia_bazi/Models/PlayersName.dart';

class PlayerRepository {

  void readAll() async{
    final names = await Amplify.DataStore.query(PlayersName.classType);
  }


  Future<List<PlayersName>> getPlayers() async {
    try {
      final List<PlayersName> playersNames = await Amplify.DataStore.query(PlayersName.classType);
      return playersNames;
    } catch (e) {
      print("Could not query DataStore");
      throw e;
    }
  }

  Future<void> addPlayer(String name) async {
    final player = PlayersName(Players: name);
    return await Amplify.DataStore.save(player);
  }

  Future<void> removePlayer(PlayersName player) async {
    return await Amplify.DataStore.delete(player);
  }
}
