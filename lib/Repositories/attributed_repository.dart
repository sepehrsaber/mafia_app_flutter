import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:mafia_bazi/Models/AttributedChars.dart';

class attributedPlayers {
  Future<List<AttributedChars>> readAll() async {
    final attributedplayers =
        await Amplify.DataStore.query(AttributedChars.classType);
    return attributedplayers;
  }

  Future<List<AttributedChars>> getAttr() async {
    try {
      final List<AttributedChars> attributedChars =
          await Amplify.DataStore.query(AttributedChars.classType);
      return attributedChars;
    } catch (e) {
      print('query problem show chars');
      throw e;
    }
  }

  Future<void> addAttr(String char, String name, String side) async {
    final newAttr =
        AttributedChars(Character: char, Side: side, PlayerName: name);
    return await Amplify.DataStore.save(newAttr);
  }
}
