import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mafia_bazi/Models/AttributedChars.dart';
import 'package:mafia_bazi/Models/PlayersName.dart';
import 'package:mafia_bazi/Repositories/attributed_repository.dart';
import 'package:mafia_bazi/Repositories/player_name_repository.dart';

abstract class ShowCharsState {}

class LoadingAttr extends ShowCharsState {}

class ListAttributes extends ShowCharsState {
  List<AttributedChars> chars;
  List<PlayersName> names;

  ListAttributes({required this.chars, required this.names});
}

class LoadFailure extends ShowCharsState {
  final String error;

  LoadFailure({required this.error});
}

class ShowAttributesCubit extends Cubit<ShowCharsState> {
  final _AttrReps = attributedPlayers();
  final _namesReps = PlayerRepository();

  ShowAttributesCubit() : super(null);

  void getAttribute() async {
    if (state is ListAttributes == false) {
      emit(LoadingAttr());
    }
      try{
        final attrs = await _AttrReps.getAttr();
        final names = await _namesReps.getPlayers();
        emit(ListAttributes(chars: attrs, names: names));
      }catch(e){
        emit(LoadFailure(error: e.toString()));
      }

  }
  void addAttribute(String name,String char,String side) async{
    await _AttrReps.addAttr(char, name, side);
    getAttribute();
  }
}
