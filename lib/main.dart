import 'package:flutter/material.dart';
import 'package:mafia_bazi/Pages/menu_page.dart';
import 'package:mafia_bazi/Pages/playername_page.dart';
import 'Pages/GamePages/game_page.dart';
import 'Pages/character_page.dart';
import 'Pages/show_chars.dart';

// Amplify Flutter Packages
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:amplify_datastore/amplify_datastore.dart';
// import 'package:amplify_api/amplify_api.dart'; // UNCOMMENT this line after backend is deployed

// Generated in previous step
import 'models/ModelProvider.dart';
import 'amplifyconfiguration.dart';

void main() {
  runApp(const _homePage());
}

class _homePage extends StatefulWidget {
  const _homePage({Key? key}) : super(key: key);

  @override
  State<_homePage> createState() => _homePageState();
}

class _homePageState extends State<_homePage> {
  bool _amplifyConfigured = false;

  @override
  void initState() {
    super.initState();
    _configureAmplify();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: _amplifyConfigured
            ? MenuPageBody()
            : Scaffold(
                body: Center(
                  child: CircularProgressIndicator(),
                ),
              ));
  }

  void _configureAmplify() async {
    // await Amplify.addPlugin(AmplifyAPI()); // UNCOMMENT this line after backend is deployed
    await Amplify.addPlugin(
        AmplifyDataStore(modelProvider: ModelProvider.instance));

    try {
      // Once Plugins are added, configure Amplify
      await Amplify.configure(amplifyconfig);

    } catch (e) {
      print(e);
    }
    setState(() {
      _amplifyConfigured = true;
    });
  }

}
